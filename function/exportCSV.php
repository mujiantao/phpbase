<?php
/**
 * 数据以csv导出
 * @param  [type] $data     [description]
 * @param  [type] $filename [description]
 * @return [type]           [description]
 */
function exportCSV($data, $filename){
    $today = date('Y-m-d');
    header("Content-type:text/csv");
    header('Expires:0');
    header("Content-Disposition: attachment;filename=$filename-$today".".csv");
    header('Cache-Control: max-age=0');
    $str = "\xEF\xBB\xBF"; //添加BOM, 防止中文乱码
    //head
    $str .= implode(',', $data['head'])."\n"; //用引文逗号分开
    //body
    foreach ($data['body'] as $body){
        $str .=  str_replace(array("\r\n", "\r", "\n"), '',implode(',', $body))."\n"; //用引文逗号分开
    }
    echo $str;
    die;
}
?>
